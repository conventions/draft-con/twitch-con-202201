<!--
- 🖐 Fill the title of the issue with the title of the proposal
- Think carefully and try to create a title that is descriptive enough.
- 👋 Don't forget to fill correctly your GitLab user profile (add a bio to your profile)
-->

## Summary of your presentation, for the attendee
<!-- 
Describe your proposal as it will appear on our website and mobile apps. 
-->


## Message for the program committee
<!--
Write here any important details about your presentation, that the program committee should know.
You can also set here the URL to your slides, or a link to an existing online talk.
-->

## Main language

- [ ] 🇫🇷
- [ ] 🇬🇧

## Type of talk

- [ ] Conference (45 mins)
- [ ] Keynote (20 mins)
- [ ] Hands-on Labs (3h)
- [ ] University (3h)
- [ ] Quickie (15 mins)
- [ ] Tools in actions (25 mins)


## Track for your proposal

- [ ] architecture
- [ ] cloud
- [ ] language
- [ ] methodology
- [ ] web

## Audience level

- [ ] beginner
- [ ] intermediate
- [ ] expert

## What type of presentation will it be?

- [ ] slides only
- [ ] code only
- [ ] slides and code
- [ ] something

## Tags

- [ ] java
- [ ] rust
- [ ] go
- [ ] javascript
- [ ] devops
- [ ] security
- [ ] microservices
- [ ] wasm
- [ ] faas
- [ ] serverless
- [ ] saas
- [ ] paas
- [ ] iaas


<!-- 🖐 don't remove -->
/confidential
